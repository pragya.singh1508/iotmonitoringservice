const cron = require('node-cron');
const produceMessages = require('./send-event');
const axios = require('axios'); 

const schedule = process.env.CRON_EXPRESSION || '* * * * *'; // Default to every minute if not set

cron.schedule(schedule, async () => {

  const eventData = {
    id: Date.now(),
    type: "threshold_breach",
    metric: "temperature",
    value: 35,
    threshold: 30,
    timestamp: Date.now()
  };

  try {
    await pushToAPI(eventData);
  } catch (error) {
    console.error('Error pushing event data:', error);
  }
});

async function pushToAPI(eventData) {
  const apiUrl = process.env.API_URL || 'http://localhost:3002/events';
  try {
    const response = await axios.post(apiUrl, eventData);
    if(response.status === 200){
      console.log("API call successful");
    }
  } catch (error) {
    console.error('Error making API request:', error);
    if(error.status && error.status >= 400 ){
      
    }
  }
}

module.exports = cron;