const http = require('http');
const app = require('./app');
require('./cron-job/cron-job');

const port = process.env.PORT || 3000;

const server = http.createServer(app);

server.listen(port);