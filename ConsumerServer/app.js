const express = require('express');
const app = express();
const consumeMessages = require('./api/read-event')

consumeMessages().catch(console.error);

module.exports = app;