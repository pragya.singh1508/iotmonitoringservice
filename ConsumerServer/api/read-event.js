const { Kafka, logLevel, LegacyPartitioner } = require('kafkajs');
const mongoose = require('mongoose');
const Breach = require('../model/threshold-breach');


mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost:27017/iot_data' , { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('Connected to MongoDB'))
    .catch(err => console.error('Failed to connect to MongoDB', err));

// Initialize a Kafka instance
const kafka = new Kafka({
  clientId: process.env.CLIENT_ID,
  brokers: [process.env.KAFKA_BROKER || 'localhost:9092'],
  logLevel: logLevel.WARN
});

// Create a consumer instance
const consumer = kafka.consumer({ groupId: 'application-server-group' });

const consumeMessages = async () => {
  await consumer.connect();
  await consumer.subscribe({ topic: 'application-server-topic', fromBeginning: true });

  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      const newBreach = new Breach(message);
      newBreach.save()
      .then(() => {
          console.log(`Received breach data: ${JSON.stringify(req.body)}`);
          res.status(200).json({ message: 'Breach data received', timestamp: newBreach.timestamp });
      })
      .catch(err => {
          console.error('Error saving breach data to DB', err);
          res.status(500).json({ message: 'Error saving breach data' });
      });
    },
  });
};

module.exports = consumeMessages;
