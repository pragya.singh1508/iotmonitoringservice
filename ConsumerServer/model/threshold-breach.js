const mongoose = require('mongoose');

const breachSchema = new mongoose.Schema({
  id: { type: Number, required: true, unique: true },
  type: { type: String, required: true },
  metric: { type: String, required: true },
  value: { type: Number, required: true },
  threshold: { type: Number, required: true },
  timestamp: { type: Number, required: true }
});

const Breach = mongoose.model('Breach', breachSchema);

module.exports = Breach;
