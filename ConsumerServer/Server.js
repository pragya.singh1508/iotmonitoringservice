const http = require('http');
const app = require('./app');
require('./api/read-event')

const port = process.env.PORT || 3008;

const server = http.createServer(app);

server.listen(port);