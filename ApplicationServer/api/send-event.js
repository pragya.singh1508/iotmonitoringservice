const { Kafka, logLevel, LegacyPartitioner } = require('kafkajs');

// Initialize a Kafka instance
const kafka = new Kafka({
  clientId: process.env.CLIENT_ID,
  brokers: [process.env.KAFKA_BROKER || 'localhost:9092'],
  logLevel: logLevel.WARN
});

const producer = kafka.producer({
    createPartitioner: LegacyPartitioner,
    retry: {
      retries: 5,
      initialRetryTime: 300,
      factor: 2,
      multiplier: 1.5,
      maxRetryTime: 5000
    }
  });

  const produceMessages = async (event) => {
    try {
      await producer.connect();
      console.log('Producer connected');
  
      const sendMessage = async () => {
        try {
          // Send the message to the Kafka topic
          await producer.send({
            topic: process.env.KAFKA_TOPIC || 'application-server-topic',
            messages: [
              { value: JSON.stringify(event) }
            ]
          });
  
          console.log('Event sent successfully', event);
        } catch (error) {
          console.error('Error sending message, retrying...', error);
          throw error;  // Propagate error to retry logic
        }
      };
  
      await sendMessage();
  
    } catch (error) {
      console.error('Failed to send event after retries', error);
    } finally {
      await producer.disconnect();
      console.log('Producer disconnected');
    }
  };
  
module.exports = produceMessages;
