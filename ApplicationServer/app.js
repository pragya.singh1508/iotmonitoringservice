const express = require('express');
const app = express();
const produceMessages = require('./api/send-event');

var bodyParser = require('body-parser')
app.use( bodyParser.json() );      
app.use(bodyParser.urlencoded({  extended: true }));

app.post('/events', (req, res, next) => {
    produceMessages(req.body).catch(console.error);
    res.status(200).json({
        message: 'Event published to the comsumer successfull',
        event: req
    });
});

module.exports = app;